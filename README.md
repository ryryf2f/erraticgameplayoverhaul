# Erratic Gameplay Overhaul
# A.K.A. EGO

Erratic Gameplay Overhaul is a plugin for 7 days to die, and it aims to change random and various things in the game to create
a more diverse, interactive, fun gameplay.

### Current Working Version
	
	```
	<Null> for version Alpha16.4b8
	```
	
### What are the changes?
	
Please view changelog.txt in the correct branch.


### How are branches organized?
	
Master contains the version of the current game, and the most stable up to date code. However, checking tags,
you can find more specific versions. Dev branches will have their game version definined, and the newest code can be found there.
	
###	Questions, comments, concerns?

Please feel free to leave tickets in the issue tracker, and the wiki will be updated once the mod begins to fully take shape.

### License

	![Creative Commons License](http://creativecommons.org/licenses/by-nc-sa/4.0/)